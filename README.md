# potential

Publisher-Subscriber (PubSub) pipework based on arc-swap (non-locking) and a linked list

Since it is a linked list, there isn't one large pile of items in a buffer, but instead
individual items spread over the heap. This has downsides - such as bloat and fragmentation 
and upsides - like size fitting into already fragmented memory.

The arc-swap makes it possible to have multiple publishers and subscribers, operating
on a single pipe. Unsubscribed pipes behave like /dev/null - you can broadcast, but nobody
is listening. We can also have pipes without publishers - subscribers will consume
what's in their pipe and then announce they are finished.

Publishers and subsribers can be cloned and we can always add publishers or subscribers
to the pipe if we have either side. A new publisher will catch up with the most recent items and 
a new subscriber will only see items published after it's been added.

There is an async based API including a stream of the subscription and we have the lower level sync
pub-sub. Publishing doesn't block, though it may spin a few times on races.

There are other, probably faster solutions for pub-sub / broadcast! 
 - async-broadcast

You may prefer this crate for it's simplicity and lean dependencies. Not for the performance.

No `unsafe` - the unsafe stuff is done in the tried and tested arc-swap.

## License
MIT OR Apache-2.0

### Contribution
Unless you explicitly state otherwise, any contribution 
submitted for inclusion in this project by you, 
as defined in the Apache-2.0 license, shall be licensed 
as above, without any additional terms or conditions.
