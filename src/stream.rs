use crate::Sub;
pub use futures_core::Stream;
use std::{
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
};

impl<T: Clone> Stream for Sub<T> {
    type Item = T;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        Sub::poll_next(&mut self, cx)
    }
}

#[test]
fn streams() {
    use futures_lite::stream::StreamExt;
    let mut publish = crate::Pub::default();
    publish.push(6); // ignored by sub
    let sub = publish.subscribe();
    publish.push(5);
    publish.push(4);
    publish.push(3);
    drop(publish);

    let results = futures_lite::future::block_on(sub.collect::<Vec<_>>());

    assert_eq!(results, vec![5, 4, 3]);
}
