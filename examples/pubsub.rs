use std::{iter, time::Instant};

use potential::sync::Pub;

fn main() {
    let mut publisher = Pub::default();
    let mut subs = publisher.subscribe();
    let  iterations = 1000000;
    let mut count = iterations;
    eprintln!("{iterations}");
    let start = Instant::now();
    for _ in 0..iterations {
        publisher.push(());
    }
    let time_pub = start.elapsed();
    let start = Instant::now();
    while let Ok(v) = subs.try_pop() {
        count -= 1;
    }
    let time_sub = start.elapsed();
    assert_eq!(count, 0);

    println!("pub {}", time_pub.as_secs_f32() / iterations as f32);
    println!("sub {}", time_sub.as_secs_f32() / iterations as f32);
}
