#![feature(test)]

use std::{io::{stderr, Write as _}, sync::{mpsc::channel, Arc}};

use arc_swap::ArcSwapOption;
use potential::sync::Pub;

extern crate test;

#[bench]
fn bench_pub_sub(bench: &mut test::Bencher) {
    let mut publisher = Pub::default();
    let mut subs = publisher.subscribe();
    let mut count = 0;

    bench.iter(|| {
        assert!(publisher.push(()));
        count += 1;
    });
    while let Ok(_) = subs.try_pop() {
        count -= 1;
    }
    assert_eq!(count, 0);
}

#[bench]
fn bench_arc_swap(bench: &mut test::Bencher) {
    #[derive(Debug, Default)]
    struct Link {
        next:Option<Box<((),Link)>>
    }

    let swapper: ArcSwapOption<((), Link)> = ArcSwapOption::empty();
    let mut old = swapper.load();
    let mut item = 0;
    bench.iter(|| {
        item += 1;
        old = swapper.compare_and_swap(&old, Some(Arc::new((Default::default(), Link::default()))));
    });
}

#[bench]
fn bench_chan(bench: &mut test::Bencher) {
    let (tx, rx) = channel();
    let mut count = 0;
    bench.iter(|| {
        tx.send(()).expect("send");
        count += 1;
    });
    while let Ok(_) = rx.try_recv() {
        count -= 1;
    }
    assert_eq!(count, 0);
}

#[bench]
fn bench_async_broadcast(bench: &mut test::Bencher) {
    let (tx, mut rx) = async_broadcast::broadcast(111111111);
    let mut count = 0;
    bench.iter(|| {
        tx.broadcast_blocking(()).expect("send");
        count += 1;
    });
    while let Ok(_) = rx.try_recv() {
        count -= 1;
    }
    assert_eq!(count, 0);
}
