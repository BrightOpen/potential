#![feature(test)]

use std::sync::mpsc::channel;

use potential::sync::Pub;

extern crate test;

#[bench]
fn bench_pub_sub(bench: &mut test::Bencher) {
    let mut publisher = Pub::default();
    let mut subs = publisher.subscribe();

    for _i in 0..111111111 {
        assert!(publisher.push(()));
    }
    bench.iter(|| {
        subs.try_pop().expect("pop");
    });
}

#[bench]
fn bench_chan(bench: &mut test::Bencher) {
    let (tx, rx) = channel();

    for _i in 0..1111111111 {
        tx.send(()).expect("send")
    }
    bench.iter(|| {
        rx.try_recv().expect("send");
    });
}

#[bench]
fn bench_async_broadcast(bench: &mut test::Bencher) {
    let (tx, mut rx) = async_broadcast::broadcast(1111111111);

    for _i in 0..1111111111 {
        tx.try_broadcast(()).expect("send");
    }
    bench.iter(|| {
        rx.try_recv().expect("send");
    });
}
